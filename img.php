<?php
require_once('global.php');

$result = "";
 
try
{
  global $db_name;
  global $db_user;
  global $db_pass;
      
  $dbh = new PDO('mysql:host=localhost;dbname=' . $db_name, $db_user, $db_pass);
  
  $sth = $dbh->prepare('SELECT * FROM files ORDER BY id DESC LIMIT 10');
  $sth->execute();
  
  $result = $sth->fetchAll();
  
  $sth = null;
  $dbh = null;
}
catch(PDOException $e)
{
  $uploadOk = 0;
  $reason = $e->getMessage();
  unlink($target_file);
}

foreach($result as $img) {
  $id = $img['id'];
  $name = $img['name'];
  $title = $img['title'];
  $desc = $img['description'];
  $date = $img['date'];
  
  echo <<<EOT
    <div class="card mb-5">
          <div class="card-body">
            <h5 class="card-title">$title</h5>
            <p class="card-text">$desc</p>
            <a href="#" class="btn btn-secondary btn-sm mb-3" data-toggle="collapse" data-target="#collapseShare-$id" aria-expanded="false" aria-controls="collapseShare-$id"><i class="fas fa-share-alt"></i></a>
            <div class="input-group mb-3 collapse" id="collapseShare-$id">
              <input type="text" class="form-control  form-control-sm" value="$base_url/$upload_dir$name" aria-label="$base_url/$upload_dir$name" aria-describedby="button-addon2" disabled="true">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary btn-sm copy" type="button"><i class="far fa-copy"></i> Copy link</button>
              </div>
            </div>
            <p class="card-text">
              <small class="text-muted">Uploaded on $date</small>
            </p>
          </div>
          <img src="$base_url/$upload_dir$name" class="card-img-top" alt="$title" title="$title">
        </div>
    
EOT;
}

<?php
require_once('global.php');

global $upload_dir;

$target_dir = $upload_dir;
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$reason = "";

$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

if($check !== false)
{
  $uploadOk = 1;
}
else
{
  $reason = "File is not an image.";
  $uploadOk = 0;
}

if(file_exists($target_file))
{
  $reason = "Sorry, file already exists.";
  $uploadOk = 0;
}

global $max_file_size_limit;

if($_FILES["fileToUpload"]["size"] > $max_file_size_limit)
{
  $reason .= "Sorry, your file is too large.";
  $uploadOk = 0;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif")
{
  $reason = "Sorry, only JPG, JPEG, PNG & GIf files are allowed.";
  $uploadOk = 0;
}

if($uploadOk == 1)
{
  if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
  {
    try
    {
      global $db_name;
      global $db_user;
      global $db_pass;
      
      $dbh = new PDO('mysql:host=localhost;dbname=' . $db_name, $db_user, $db_pass);
      
      $name = $_FILES["fileToUpload"]["name"];
      $today = date('Y-m-d');
      $title = $_POST['imageTitle'];
      $desc  = $_POST['imageDescription'];
      
      $sth = $dbh->prepare('INSERT INTO files (name, title, description, date) VALUES (?, ?, ?, ?)');
      /* $sth->bindParam(1, $name);
      $sth->bindParam(2, $title);
      $sth->bindParam(3, $desc);
      $sth->bindParam(4, $today); */
      
      $sth->execute(array($name, $title, $desc, $today));
      
      $sth = null;
      $dbh = null;
    }
    catch(PDOException $e)
    {
      $uploadOk = 0;
      $reason = $e->getMessage();
      unlink($target_file);
    }
  }
  else
  {
    $reason = "Sorry, there was an error uploading your file.";
    $uploadOk = 0;
  }
}

$output = "";

if($uploadOk == 1)
{
  $name = $_FILES["fileToUpload"]["name"];
  $output .= "<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">The file <a href=\"/uploads/" . rawurlencode($_FILES["fileToUpload"]["name"]) . "\" target=\"_blank\">" . $name . "</a> was uploaded.";
}
else
{
  $output .= "<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">The file could not be uploaded. " . $reason;
}

$output .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-lable=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>";

echo $output;
<?php
  require_once('global.php');
  
  global $site_name;
  global $base_url;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js" integrity="sha384-3Nqiqht3ZZEO8FKj7GR1upiI385J92VwWNLj+FqHxtLYxd9l+WYpeqSOrLh0T12c" crossorigin="anonymous"></script>
    <title>HTML &ndash; Upload a file</title>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="<?php echo $base_url; ?>/"><?php echo $site_name; ?></a>
        
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
          <i class="fas fa-upload"></i> Upload image
        </button>
      </nav>
      
      <div class="collapse" id="collapseExample">
        <h2>Upload your image</h2>
        <form action="ul.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="imageTitle">Title for your image</label>
            <input type="text" class="form-control" id="imageTitle" name="imageTitle">
          </div>
          <div class="form-group">
            <label for="imageDescription">Description of your image</label>
            <textarea class="form-control" id="imageDescription" name="imageDescription" rows="3"></textarea>
          </div>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="testFile" name="fileToUpload">
            <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose file</label>
          </div>
          <button type="submit" class="btn btn-success mt-3">Upload</button>
        </form>
        <div class="row mt-3 mb-3"><div class="col" id="status"></div></div>
      </div>
      <div class="row">
        <div class="col">
          <h2>Here are your images</h2>
          <div id="images"></div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha512-YUkaLm+KJ5lQXDBdqBqk7EVhJAdxRnVdT2vtCzwPHSweCzyMgYV/tgGF4/dCyqtCC2eCphz0lRQgatGVdfR0ww==" crossorigin="anonymous"></script>
    <script>
$(function() {
  $('form').ajaxForm({
    beforeSend: function() {
      $("#status").empty();
    },
    uploadProgress: function(event, position, total) {
      $("status").empty();
      $("#status").html("<div class=\"progress\"><div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"" + total + "\" aria-valuemin=\"0\" aria-valuemax=\"100\">" + total + "%</div></div>");
    },
    complete: function(xhr) {
      $("#status").empty();
      $("#status").html(xhr.responseText);
      
        $.ajax({ type: "GET",
            async: false,
            url: "<?php echo $base_url; ?>/img.php",
            success: function(text)
            {
                $('#images').html(text);
            }
        });
        
        $('.copy').click(function() {
          var test = $(this).parent().siblings('.form-control')[0];
          test.disabled=false;
          test.select();
          document.execCommand("copy");
          test.disabled=true;
        });
    }
  });
  
  $.ajax({ type: "GET",
            async: false,
            url: "<?php echo $base_url; ?>/img.php",
            success: function(text)
            {
                $('#images').html(text);
            }
  });
  
  $('[data-toggle="tooltip"]').tooltip();
  
  $('.copy').click(function() {
    var test = $(this).parent().siblings('.form-control')[0];
    test.disabled=false;
    test.select();
    document.execCommand("copy");
    test.disabled=true;
  });
});


    </script>
  </body>
</html>